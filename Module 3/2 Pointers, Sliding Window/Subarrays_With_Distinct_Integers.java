import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int a[] = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      int b = sc.nextInt();
      System.out.println(countAtMost(a, b, n) - countAtMost(a, b-1, n));
    }
     
  static int countAtMost(int[] a, int b, int n){
     Map<Integer, Integer> mp = new HashMap<>();
      int i=0, j = 0, cnt = 0, ans = 0;
      while(j < n){
        if(!mp.containsKey(a[j])){
          mp.put(a[j], 1);
          cnt++;
        }else mp.put(a[j], mp.get(a[j]) + 1);
        while(cnt > b){
          mp.put(a[i], mp.get(a[i]) - 1);
          if(mp.get(a[i]) == 0){
            mp.remove(a[i]);
            cnt--;
          }
          i++;
        }
        ans += j-i+1;
        j++;
      }
      return ans;
	}
}