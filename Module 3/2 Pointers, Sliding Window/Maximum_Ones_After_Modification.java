import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int a[] = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      int b = sc.nextInt();
      int i=0, j=0, cnt = 0, ans = 0;
      while(j<n){
        if(a[j] == 1)j++;
        else if(a[j] == 0 && cnt < b){
          j++;
          cnt++;
        }
        else if(a[j] == 0){
          if(a[i] == 0)cnt--;
          i++;
          if(i==j){
            i++;
            j++;
          }
        }
        ans = Math.max(ans, j-i);
      }
      System.out.println(ans);
		//your code here
	}
}