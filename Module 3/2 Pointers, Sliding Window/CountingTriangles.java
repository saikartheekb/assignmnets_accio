import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int a[] = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      Arrays.sort(a);
      long ans = 0;
      long mod = (int)1e9+7;
      for(int i=0; i<n; i++){
        for(int j=i+1; j<n; j++){
          int x = lowerBound(a, j+1, n-1, a[i] + a[j]) - j - 1;
          ans += x;
          ans %= mod;
        }
      }
      System.out.println(ans);
		//your code here
	}
  static int lowerBound(int[] a, int l, int r, int k){
    int ans = r+1;
    while(l<=r){
      int mid = (l+r)/2;
      if(a[mid] >= k){
        ans = mid;
        r = mid - 1;
      }else l = mid + 1;
    }
    return ans;
  }
}














