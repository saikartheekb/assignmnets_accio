import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int a[] = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      int k = sc.nextInt();
      Map<Integer, Integer> mp = new HashMap<>();
      for(int i=0; i<n; i++){
        if(mp.containsKey(k+a[i]) || mp.containsKey(a[i]-k)){
          System.out.println(1);
          return;
        }
        mp.put(a[i], 1);
      }
      System.out.println(0);
		//your code here
	}
}