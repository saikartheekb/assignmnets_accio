import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int k = sc.nextInt();
      int a[] = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      int pre[] = new int[n];
      pre[0] = (a[0] % k + k) % k;
      for(int i=1; i<n; i++){
        pre[i] = ((pre[i-1] + a[i]) % k + k) % k;
      }
      int ans = 0;
      HashMap<Integer, Integer> mp = new HashMap<>();
      mp.put(0, 1);
      for(int i=0; i<n; i++){
        if(mp.containsKey(pre[i])){
          ans += mp.get(pre[i]);
          mp.put(pre[i], mp.get(pre[i])+1);
        }else mp.put(pre[i], 1);
      }
      System.out.println(ans);
		//your code here
	}
}


//----------------- O(n^2)------------------------//

// import java.util.*;
// import java.lang.*;
// import java.io.*;

// public class Main
// {
// 	public static void main (String[] args) throws java.lang.Exception
// 	{
//       Scanner sc = new Scanner(System.in);
//       int n = sc.nextInt();
//       int k = sc.nextInt();
//       int a[] = new int[n];
//       for(int i=0; i<n; i++){
//         a[i] = sc.nextInt();
//       }
//       int pre[] = new int[n+1];
//       for(int i=1; i<=n; i++){
//         pre[i] = pre[i-1] + a[i-1];
//       }
//       int cnt = 0;
//       for(int i=0; i<n; i++){
//         for(int j=i; j<n; j++){
//           int sum = pre[j+1]-pre[i];
//           if(sum%k == 0)cnt++;
//         }
//       }
//       System.out.println(cnt);
// 		//your code here
// 	}
// }