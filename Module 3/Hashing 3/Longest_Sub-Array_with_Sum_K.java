import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int k = sc.nextInt();
      int a[] = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      int pre[] = new int[n];
      pre[0] = a[0];
      for(int i=1; i<n; i++){
        pre[i] = pre[i-1] + a[i];
      }
      int ans = 0;
      Map<Integer, Integer> mp = new HashMap<>();
      mp.put(0, -1);
      for(int i=0; i<n; i++){
        if(mp.containsKey(pre[i] - k)){
          int left = mp.get(pre[i] - k);
          ans = Math.max(ans, i-left);
        }
        if(!mp.containsKey(pre[i])){
          mp.put(pre[i], i);
        }
      }
      System.out.println(ans);
		//your code here
	}
}