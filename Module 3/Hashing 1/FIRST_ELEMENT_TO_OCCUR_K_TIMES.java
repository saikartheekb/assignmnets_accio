import java.io.*;
import java.util.*;
public class Main {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt(), k = input.nextInt();
        int a[] = new int[n], freq[] = new int[1000001];
        for(int i = 0; i < n; i++){
            a[i] = input.nextInt();
        }   
        for(int i = 0; i < n; i++){
            freq[a[i]]++;
            if(freq[a[i]] == k){
                System.out.println(a[i]);
                return;
            }
        }
        System.out.println(-1);
    }
}