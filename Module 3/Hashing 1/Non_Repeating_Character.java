import java.io.*;
import java.util.*;
public class Main {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        int f[] = new int[26];
        String s = input.next();
        int n = s.length();
        for(int i = 0; i < n; i++){
            f[s.charAt(i) - 'a']++;
        }
        for(int i = 0; i < n; i++){
            if(f[s.charAt(i)-'a'] == 1){
                System.out.println(s.charAt(i));
                return;
            }
        }
        System.out.println(-1);
    }
}