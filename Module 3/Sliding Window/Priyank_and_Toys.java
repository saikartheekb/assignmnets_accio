import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int a[] = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      Arrays.sort(a);
      int ans = 1, min = a[0];
      for(int i=0; i<n; i++){
        if(a[i] > min + 4){
          ans++;
          min = a[i];
        }
      }
      System.out.println(ans);
		//your code here
	}
}