import java.util.*;
import java.lang.*;
import java.io.*;

public class Leap_Year
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int y = sc.nextInt();
      int ans;
      if(y%100==0 && y%400!=0) ans = 0;
      else if(y%4 == 0) ans = 1;
      else ans = 0;
      System.out.println(ans);
    }
}