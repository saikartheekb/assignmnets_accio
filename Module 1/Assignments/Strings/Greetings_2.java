import java.util.*;
import java.lang.*;
import java.io.*;

public class Greetings_2
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      String s = sc.nextLine();

      String ans = new String();
      for(int i=0; i<s.length(); i++){
        if(s.charAt(i) == 'e')ans += "ee";
        else ans += s.charAt(i);
      }

      /* --using StringBuilder
      StringBuilder ans = new StringBuilder("");
      for(int i=0; i<s.length(); i++){
        if(s.charAt(i) == 'e')ans.append("ee");
        else ans.append(s.charAt(i));
      }
      */
      
      System.out.println(ans);
	}
}