import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
    static int solve(int[] a, int i){
      if(i == a.length) return 0;
      if(a[i] == 11)return 1+solve(a, i+1);
      return solve(a, i+1);
    }
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int[] a = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      System.out.println(solve(a, 0));
		//your code here
	}
}
