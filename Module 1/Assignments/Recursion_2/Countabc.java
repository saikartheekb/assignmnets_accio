import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
    static int countAbc(String s, int i){
      if(i >= s.length()-2) return 0;
      String temp = s.substring(i, i+3);
      int cnt = 0;
      if(temp.compareTo("abc") == 0) cnt++;
      if(temp.compareTo("aba") == 0) cnt++;
      return cnt + countAbc(s, i+1);
    }
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      String s = sc.nextLine();
      System.out.println(countAbc(s, 0));
		//your code here
	}
}
