import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
    static String nox(String s){
      // System.out.println(s);
      if(s.length() == 0)return "";
      if(s.charAt(0) == 'x' || s.charAt(0) == 'X')return nox(s.substring(1));
      return s.charAt(0) + nox(s.substring(1));
    }
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      String s = sc.nextLine();
      System.out.println(nox(s));
	}
}
