import java.util.*;  
public class Rotate_a_matrix_by_90_degrees   
{
public static void main(String args[])  
{  
        Scanner sc= new Scanner(System.in);

        int n = sc.nextInt();
        int m = sc.nextInt();
        
        int[][] matrix = new int[n][m];
        
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<m;j++)
                matrix[i][j] = sc.nextInt();
        }
        
        //int n = matrix.length;
        for (int i = 0; i < (n + 1) / 2; i ++) {
            for (int j = 0; j < n / 2; j++) {
                int temp = matrix[n - 1 - j][i];
                matrix[n - 1 - j][i] = matrix[n - 1 - i][n - j - 1];
                matrix[n - 1 - i][n - j - 1] = matrix[j][n - 1 -i];
                matrix[j][n - 1 - i] = matrix[i][j];
                matrix[i][j] = temp;
            }
        }
        
        for(int i=0;i<n;i++)
        {
                for(int j=0;j<m;j++)
                        System.out.print(matrix[i][j]+" ");
                System.out.println();
        }
    }  
}  
