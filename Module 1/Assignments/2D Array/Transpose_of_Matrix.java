import java.util.*;
import java.lang.*;
import java.io.*;

public class Transpose_of_Matrix
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int[][] a = new int[n][n];
      for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
          a[i][j] = sc.nextInt();
        }
      }
      for(int i=0; i<n; i++){
        for(int j=i+1; j<n; j++){
          int x = a[i][j];
          a[i][j] = a[j][i];
          a[j][i] = x;
        }
      }
      for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
          System.out.print(a[i][j] + " ");
        }
        System.out.println();
      }
	}
}
