import java.util.*;
import java.lang.*;
import java.io.*;

public class Reverse_digits_of_a_Number
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);

      int n = sc.nextInt();
      int ans = 0;
      while(n > 0){
        ans *= 10;
        ans += n % 10;
        n /= 10;
      }
      System.out.println(ans);
      
      // StringBuilder s = new StringBuilder("");
      // s.append(sc.nextLine());
      // s.reverse();
      // System.out.println(s);
	}
}