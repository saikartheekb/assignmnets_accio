import java.util.*;
import java.lang.*;
import java.io.*;

public class PEAK_ELEMENT
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int[] a = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      int ans = -1;
      for(int i=0; i<n; i++){
        if(i==0){
          if(a[i] > a[i+1]){
            ans = i;
            break;
          }
        }else if(i==n-1){
          if(a[i] > a[i-1]){
            ans = i;
            break;
          }
        }else{
          if(a[i] > a[i+1] && a[i] > a[i-1]){
            ans = i;
            break;
          }
        }
      }
      System.out.println(ans);
	}
}