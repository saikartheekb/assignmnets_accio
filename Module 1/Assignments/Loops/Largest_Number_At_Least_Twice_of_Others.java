import java.util.*;
import java.lang.*;
import java.io.*;

public class Largest_Number_At_Least_Twice_of_Others
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int[] a = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      int mx_idx = 0;
      for(int i=0; i<n; i++){
        if(a[i] > a[mx_idx])mx_idx = i;
      }
      for(int i=0; i<n; i++){
        if(i == mx_idx)continue;
        if(a[i]*2 > a[mx_idx]){mx_idx = -1; break;}
      }
      System.out.println(mx_idx);
	}
}