import java.util.*;
import java.lang.*;
import java.io.*;

public class HRECURS_Hello_Recursion
{
  public static int sum(int a[],int n){
    if(n==0){
      return a[0];
    }
    return a[n]+sum(a,n-1);
  }
  
	public static void main (String[] args) throws java.lang.Exception
	{
	Scanner sc =new Scanner(System.in);
      int x =sc.nextInt();  //no of arrays
      for(int i=1;i<=x;i++){
        int n =sc.nextInt();   //array size
        int []arr1 =new int[n];
        for(int j=0;j<n;j++){//inputs
          arr1[j] = sc.nextInt();
        }
        System.out.println("Case "+ i+": "+sum(arr1,n-1));	
        }
      }
}
