import java.util.*;
import java.lang.*;
import java.io.*;

public class Recursive_Fibonacci
{

  static int fibonacci(int n){
    if(n == 1)return 0;
    if(n == 2)return 1;
    return fibonacci(n-1) + fibonacci(n-2);
  }
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      System.out.println(fibonacci(n));
		//your code here
	}
}