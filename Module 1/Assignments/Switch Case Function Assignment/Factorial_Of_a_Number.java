import java.util.*;
import java.lang.*;
import java.io.*;

public class Factorial_Of_a_Number {
  public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
    Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      long fact = 1;
      for(int i=1; i<=n; i++)fact*=i;
      System.out.print(fact);
	}
    
}
