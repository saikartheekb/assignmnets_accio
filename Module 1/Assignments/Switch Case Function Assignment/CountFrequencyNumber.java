import java.util.*;
import java.lang.*;
import java.io.*;

public class CountFrequencyNumber {
    public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int d = sc.nextInt();
      int ans = 0;
      while(n>0){
        if(n%10 == d)ans++;
        n/=10;
      }
      System.out.print(ans);
	}
}
