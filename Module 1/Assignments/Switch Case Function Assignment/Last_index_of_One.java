import java.util.*;
import java.lang.*;
import java.io.*;


public class Last_index_of_One {
  public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc=new Scanner(System.in);
      String s=sc.nextLine();
      int n=s.length();
      int ans = -1;
      for(int i=n-1; i>=0; i--){
        if(s.charAt(i)=='1'){ans=i; break;}
      }
      System.out.print(ans);
	}
}
