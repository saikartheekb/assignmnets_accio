import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int m = sc.nextInt();
      int[][] a = new int[n][m];
      for(int i=0; i<n; i++){
        for(int j=0; j<m; j++){
          a[i][j] = sc.nextInt();
        }
      }
      for(int j=0; j<m; j++){
        for(int i=0; i<n; i++){
          System.out.print(a[i][j] + " ");
        }
        System.out.println();
      }
	}
}