import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      String s = sc.next();
      char c = sc.next().charAt(0);
      int ans = -1;
      for(int i=0; i<s.length(); i++){
        if(s.charAt(i)==c){
          ans = i+1;
          break;
        }
      }
      System.out.println(ans);
		//your code here
	}
}