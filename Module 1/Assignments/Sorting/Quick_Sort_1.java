import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int[] a = new int[n], b = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      int k = 0;
      for(int i=0; i<n; i++){
        if(a[i] < a[0])b[k++] = a[i];
      }
      for(int i=0; i<n; i++){
        if(a[i] == a[0])b[k++] = a[i];
      }
      for(int i=0; i<n; i++){
        if(a[i] > a[0])b[k++] = a[i];
      }
      for(int i=0; i<n; i++){
        System.out.print(b[i] + " ");
      }
		//your code here
	}
}