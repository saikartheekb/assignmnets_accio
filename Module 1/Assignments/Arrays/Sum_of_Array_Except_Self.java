import java.util.*;
import java.lang.*;
import java.io.*;

public class Sum_of_Array_Except_Self
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a= new int[n];
        int sum = 0;
        for(int i=0; i<n; i++){
            a[i] = sc.nextInt();
            sum += a[i];
        }
        for(int i=0; i<n; i++){
            int x = sum - a[i];
            System.out.print(x + " ");
        }
      
	}
}