import java.util.*;
import java.lang.*;
import java.io.*;

public class FACING_THE_SUN
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc=new Scanner(System.in);
      int n=sc.nextInt();
      int a[]=new int[n];
      for(int i=0;i<n;i++){
        a[i]=sc.nextInt();
      }
      
      int cnt=0;
      int mx=a[0];
      
      for(int i=0;i<n;i++){
        mx = Math.max(a[i], mx);
        if(mx == a[i])cnt++;
      }
      System.out.println(cnt);
	}
}