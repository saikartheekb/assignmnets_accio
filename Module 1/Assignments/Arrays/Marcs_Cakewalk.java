import java.util.*;
import java.lang.*;
import java.io.*;

public class Marcs_Cakewalk
{
  static long solve(int arr[],int n){
    
    //arr=4,2,5,1,3,4
    Arrays.sort(arr);// sorting function
    //arr=1,2,3,4,4,5
    
    for(int i=0;i<n/2;i++){ //swapping 
      int x=arr[i];
      arr[i]=arr[n-i-1];
      arr[n-i-1]=x;
    }
    //arr=5,4,4,3,2,1
    
    long ans=0;
    for(int i=0;i<n;i++){
      
      ans+=(Math.pow(2,i)*arr[i]);
    }
    
    return ans;
  }
  
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc=new Scanner(System.in);
      int n=sc.nextInt();
      int arr[]=new int[n];
      for(int i=0;i<n;i++){
        arr[i]=sc.nextInt();
      }
      System.out.println(solve(arr,n));
	}
}