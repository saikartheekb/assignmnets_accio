import java.util.*;
import java.lang.*;
import java.io.*;

public class Divisible_Sum_Pairs
{
   static int solve(int arr[],int k){
     int n=arr.length;
     int cnt = 0;
        for(int i=0; i<n; i++){
          for(int j=i+1; j<n; j++){
            if((arr[i] + arr[j]) % k == 0)
              cnt++;
          }
        }
     return cnt;
   }
  
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int k = sc.nextInt();
      int[] a = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      
      System.out.println(solve(a,k));
	}
}