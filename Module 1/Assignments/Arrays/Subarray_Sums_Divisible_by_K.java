import java.util.*;
import java.lang.*;
import java.io.*;

public class Subarray_Sums_Divisible_by_K
{
  
	public static void main (String[] args) throws java.lang.Exception
	{
		Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k=sc.nextInt();
        int[] a= new int[n];
        for(int i=0; i<n; i++){
            a[i] = sc.nextInt();
        }
        int cnt = 0;
        for(int l=0; l<n; l++){
          for(int r=l; r<n; r++){
            long sum = 0;
            for(int i=l; i<=r; i++){
              sum += a[i];
            }
            if(sum % k == 0)cnt++;
          }
        }
        // for(int i=0; i<n; i++){
        //   for(int j=i+1; j<n; j++){
        //     if((a[i]+a[j])%k==0)cnt++;
        //   }
        // }
        System.out.println(cnt);
	}
}