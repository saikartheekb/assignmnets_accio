import java.util.*;
import java.lang.*;
import java.io.*;

public class Searching_a_number
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int k = sc.nextInt();
      int[] a = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      int ans = -1;
      for(int i=0; i<n; i++){
        if(a[i] == k){ans = i+1; break;}
      }
      System.out.println(ans);
	}
}