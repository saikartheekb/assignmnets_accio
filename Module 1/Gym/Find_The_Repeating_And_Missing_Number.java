import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int[] a = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      Arrays.sort(a);
      int repeating = 0, sum = 0;
      for(int i=0; i<n-1; i++){
        if(a[i] == a[i+1]){
          repeating = a[i];
        }
        sum += a[i];
      }
      sum += a[n-1] - repeating;
      int missing = ((n*(n+1))/2) - sum;
      System.out.println(repeating + " " + missing);
		//your code here
	}
}