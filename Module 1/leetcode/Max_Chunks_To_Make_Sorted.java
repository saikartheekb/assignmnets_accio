//Max Chunks To Make Sorted
//https://leetcode.com/problems/max-chunks-to-make-sorted/

class Solution {
	public:
	int maxChunksToSorted(vector<int> & arr) {
		int ans = 0;
		// int sum=0, arr_sum=0;
		int curr_max = 0;
		for (int i = 0; i < arr.size(); i++) {
			// sum+=i;
			// arr_sum+=arr[i];
			curr_max = max(curr_max, arr[i]);
			if (i == curr_max) {
				ans++;
			}
		}
		return ans;
	}
}